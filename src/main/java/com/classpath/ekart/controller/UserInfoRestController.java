package com.classpath.ekart.controller;

import java.text.DateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/userinfo")
@RequiredArgsConstructor
public class UserInfoRestController {

	private final OAuth2AuthorizedClientService clientService;

	@GetMapping
	public Map<String, String> userInfo(OAuth2AuthenticationToken authToken) {

		OAuth2AuthorizedClient authorizedClient = this.clientService.loadAuthorizedClient(
				authToken.getAuthorizedClientRegistrationId(), authToken.getPrincipal().getName());

		Map<String, String> response = new LinkedHashMap<>();

		if (authorizedClient != null) {
			OAuth2AccessToken oAuth2AccessToken = authorizedClient.getAccessToken();

			String oAuth2Token = oAuth2AccessToken.getTokenValue();
			String expiresAt = oAuth2AccessToken.getExpiresAt().atZone(ZoneId.systemDefault()).toLocalDate()
					.toString();
			String issuedAt = oAuth2AccessToken.getIssuedAt().atZone(ZoneId.systemDefault()).toLocalDate()
					.toString();

			String scopes = oAuth2AccessToken.getScopes().toString();

			response.put("access-token", oAuth2Token);
			response.put("issued-at", issuedAt);
			response.put("expires-at", expiresAt);
			response.put("scopes", scopes);
		}

		return response;
	}

}
