package com.classpath.ekart.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	@Override
	public void configure(HttpSecurity httpSecurity) throws Exception{
		httpSecurity.cors().disable();
		httpSecurity.csrf().disable();
		httpSecurity
		    .authorizeRequests()
		    .antMatchers("login/**", "/logut**", "/sign-up**", "/contact**")
		      .permitAll()
		    .anyRequest()
		       .authenticated()
		     .and()
		       .oauth2Login()
		       .redirectionEndpoint()
		         .baseUri("/authorization-code/callback");
	}

}
